/**
 * Blue Days Black Nights.
 * 
 * @author @deconbatch
 * @version 0.1
 * Processing 3.2.1
 * created 2019.08.22
 * an animation of Vector Field with discontinuity value.
 */

void setup() {

  size(720, 720);
  colorMode(HSB, 360, 100, 100, 100);
  rectMode(CENTER);
  smooth();
  noStroke();
  noLoop();

}

void draw() {

  int   ptnMax  = 2;      // make 2 patterns animation
  int   frmMax  = 24 * 3; // make 24fps x 3s animation image with every pattern
  float plotDiv = 0.002;
  float initDiv = 0.005;
  float baseHue = random(360.0);
  
  translate(width * 0.5, height * 0.5);
  for (int ptnCnt = 0; ptnCnt < ptnMax; ptnCnt++) {

    float noiseMult = random(5.0, 8.0);
    float plotMult  = random(3.0, 5.0);

    baseHue += 120.0;
    noiseSeed(floor(baseHue));

    // PVector[] p = {mutable plot point, immutable initial point};
    ArrayList<PVector[]> pAry = new ArrayList<PVector[]>();
    for (float xInit = -0.5; xInit <= 0.5; xInit += initDiv) {
      for (float yInit = -0.5; yInit <= 0.5; yInit += initDiv) {
        PVector[] p = {new PVector(xInit, yInit), new PVector(xInit, yInit)};
        pAry.add(p);
      }
    }

    background(0.0, 0.0, 90.0, 100);
    for (int frmCnt = 0; frmCnt < frmMax; frmCnt++) {

      blendMode(DIFFERENCE);
      float frmRatio = easeInOutCubic(map(frmCnt, 0, frmMax, 0.0, 1.0));
      float eSat     = map(sin(PI * frmRatio), 0.0, 1.0, 40.0, 80.0);
      float eBri     = map(sin(TWO_PI * frmRatio), -1.0, 1.0, 0.0, 10.0);
      float eSiz     = map(frmRatio, 0.0, 1.0, 0.8, 0.0);
      int   plotMax  = floor(map(frmRatio, 0.0, 1.0, 5.0, 20.0));

      // draw vector field
      for (int plotCnt = 0; plotCnt < plotMax; plotCnt++) {

        for (PVector[] p : pAry) {

          float xPlot = p[0].x;
          float yPlot = p[0].y;
          float xInit = p[1].x;
          float yInit = p[1].y;
          
          // make discontinuity value with floor
          float pNoise = noise(
                               floor((xInit + yInit) * noiseMult),
                               floor(xPlot * noiseMult),
                               floor(yPlot * noiseMult)
                               );
          xPlot += plotDiv * cos(TWO_PI * pNoise * plotMult);
          yPlot += plotDiv * sin(TWO_PI * pNoise * plotMult);
          
          float eHue  = baseHue + frmRatio * 60.0 + floor((xInit * yInit * 30.0) % 4.0) * 10.0;
          fill(eHue % 360.0, eSat, eBri, 100.0);
          ellipse(xPlot * width * 0.5, yPlot * height * 0.5, eSiz, eSiz);

          p[0].x = xPlot;
          p[0].y = yPlot;

        }
      }
      
      blendMode(BLEND);
      casing();
      saveFrame("frames/" + String.format("%02d", ptnCnt) + String.format("%04d", frmCnt) + "00.png");

    }

    // for still image of completed shape
    for (int i = 0; i < 12; i++) {
      saveFrame("frames/" + String.format("%02d", ptnCnt) + "9999" + String.format("%02d", i) + ".png");
    }
  }

  // for Twitter thumnail image
  saveFrame("frames/00000000.png");
  exit();
  
}

/**
 * casing : draw fancy casing
 */
private void casing() {

  fill(0.0, 0.0, 0.0, 0.0);
  strokeWeight(42.0);
  stroke(0.0, 0.0, 0.0, 100.0);
  rect(0.0, 0.0, width, height);
  strokeWeight(40.0);
  stroke(0.0, 0.0, 100.0, 100.0);
  rect(0.0, 0.0, width, height);
  noStroke();
  noFill();

}


/**
 * easeInOutCubic easing function.
 * @param  _t    0.0 - 1.0 : linear value.
 * @return float 0.0 - 1.0 : eased value.
 */
private float easeInOutCubic(float _t) {
  _t *= 2.0;
  if (_t < 1.0) {
    return pow(_t, 3) / 2.0;
  }
  _t -= 2.0;
  return (pow(_t, 3) + 2.0) / 2.0;
}
